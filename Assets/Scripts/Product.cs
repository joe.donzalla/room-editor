using Lean.Touch;
using UnityEngine;

public class Product : LeanSelectableByFinger
{
    public string productName;
    public float productPrice;

    public Sprite productPreview;
}
