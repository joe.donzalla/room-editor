using Lean.Common;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class MainController : MonoBehaviour
{
    public LeanSelect leanSelect;

    public ARPlaneManager planeManager;
    public ARRaycastManager raycastManager;

    public GameObject homeInterface;
    public GameObject shopInterface;
    public GameObject placeInterface;

    public GameObject loadingState;
    public GameObject detectedState;

    public Image toggleButton;

    public Sprite toggleActive;
    public Sprite toggleInactive;

    public TMP_Text productNameText;
    public TMP_Text productPriceText;

    public Product[] products;

    public Transform productsContent;
    public UIProduct productsPrefab;

    private Product selectedProduct;

    void Start()
    {
        foreach (var product in products)
        {
            Instantiate(productsPrefab, productsContent).SetProduct(product);
        }
    }

    void Awake()
    {
        planeManager.planesChanged += OnPlaneChanged;
    }

    /**
     * ARCore events.
     */

    private void OnPlaneChanged(ARPlanesChangedEventArgs args)
    {
        bool hasDetected = planeManager.trackables.count > 0;

        loadingState.SetActive(!hasDetected);
        detectedState.SetActive(hasDetected);
    }

    /**
     * Lean events.
     */

    public void OnProductSelected(LeanSelectable selectable)
    {
        selectedProduct = (Product)selectable;

        ShowPlaceInterface((Product)selectable);
    }

    /**
     * Private methods.
     */

    private void ShowHomeInterface()
    {
        homeInterface.SetActive(true);

        shopInterface.SetActive(false);
        placeInterface.SetActive(false);
    }

    private void ShowShopInterface()
    {
        shopInterface.SetActive(true);

        homeInterface.SetActive(false);
        placeInterface.SetActive(false);
    }

    private void ShowPlaceInterface(Product product)
    {
        productNameText.text = product.productName;
        productPriceText.text = "$" + product.productPrice.ToString();

        placeInterface.SetActive(true);

        homeInterface.SetActive(false);
        shopInterface.SetActive(false);
    }

    /**
     * Public methods.
     */

    public void SpawnProduct(Product spawnableProduct)
    {
        if (selectedProduct != null)
        {
            return;
        }

        var hits = new List<ARRaycastHit>();

        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));

        if (raycastManager.Raycast(screenCenter, hits, TrackableType.PlaneWithinPolygon))
        {
            Product selectableProduct = Instantiate(spawnableProduct, hits[0].pose.position, Quaternion.identity);

            leanSelect.Select(selectableProduct);
        }
    }

    /**
     * Click events.
     */

    public void OnToggleClick()
    {
        planeManager.enabled = !planeManager.enabled;

        if (planeManager.enabled)
        {
            toggleButton.sprite = toggleActive;
        }
        else
        {
            toggleButton.sprite = toggleInactive;
        }

        foreach (var plane in planeManager.trackables)
        {
            plane.gameObject.SetActive(planeManager.enabled);
        }
    }

    public void OnCleanClick()
    {
        LeanSelectable[] products = FindObjectsOfType<LeanSelectable>();

        foreach (var product in products)
        {
            Destroy(product.gameObject);
        }

        selectedProduct = null;
    }

    public void OnAddClick()
    {
        ShowShopInterface();
    }

    public void OnCloseClick()
    {
        ShowHomeInterface();
    }

    public void OnPlaceConfirm()
    {
        if (selectedProduct != null)
        {
            leanSelect.Deselect(selectedProduct);

            selectedProduct = null;
        }

        ShowHomeInterface();
    }

    public void OnPlaceCancel()
    {
        if (selectedProduct != null)
        {
            Destroy(selectedProduct.gameObject);

            selectedProduct = null;
        }

        ShowHomeInterface();
    }
}
