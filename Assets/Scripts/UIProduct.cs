using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIProduct : MonoBehaviour
{
    public TMP_Text nameText;
    public TMP_Text priceText;
    public Image previewImage;

    private Product product;

    private MainController controller;

    void Awake()
    {
        controller = FindObjectOfType<MainController>();
    }

    public void SetProduct(Product _product)
    {
        product = _product;

        nameText.text = _product.productName;
        priceText.text = "$" + _product.productPrice.ToString();
        previewImage.sprite = _product.productPreview;
    }

    public void OnSelectClick()
    {
        controller.SpawnProduct(product);
    }
}
